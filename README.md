# TestVagrant Mobile Hiring Challege

## Process:

In this repo, you will find two files:
  * app-debug.apk - APK to test for the hiring challenge
  * TestVagrant Android - Hiring Challenge.pdf - Instructions on how to solve the challenge


### NOTE: Please go over the instructions carefully.
